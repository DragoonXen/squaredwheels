//
// Created by dragoon on 9/1/18.
//

#ifndef MADSQUAREDWHEELS_CONSTANTS_H
#define MADSQUAREDWHEELS_CONSTANTS_H


class Constants {
public:
    static const int MAX_WIDTH = 1200;
    static const int MAX_HEIGHT = 800;
    static const int TICKS_TO_DEADLINE = 600;
};


#endif //MADSQUAREDWHEELS_CONSTANTS_H
