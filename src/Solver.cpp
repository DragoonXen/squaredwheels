//
// Created by dragoon on 3/27/18.
//
#ifdef REWIND_VIEWER

#include "../rewind/RewindClient.h"

#endif

#include "Solver.h"
#include "SimpleLog.h"
#include "Utils.h"
#include "InputParser.h"

#define _USE_MATH_DEFINES

#include <math.h>

Command Solver::onTick(ExtContext &context, json &input) {
    InputParser::parse(context, input);

#ifdef REWIND_VIEWER
    {
        RewindClient &instance = RewindClient::instance();
    }
#endif
    if (context.tickIndex <= 200) {
        return Command(MoveDirection::Stop, "tick index: " + std::to_string(context.getTickIndex()));
    }
//    return Command(static_cast<MoveDirection>(rand() % 3), "tick index: " + std::to_string(context.getTickIndex()));
    return Command(MoveDirection::Right, "tick index: " + std::to_string(context.getTickIndex()));
}