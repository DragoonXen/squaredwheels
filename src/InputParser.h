//
// Created by dragoon on 3/29/18.
//

#ifndef ALMOSTAGARIO_INPUTPARSER_H
#define ALMOSTAGARIO_INPUTPARSER_H

#include "../nlohmann/json.hpp"
#include "Context.h"
#include "SimpleLog.h"
#include "Utils.h"

using json = nlohmann::json;

class InputParser {

private:
    static void parseCar(ExtContext &context, json &input) {
        if (input[2] == 1) {
            context.leftCar.readJson(input);
        } else {
            context.rightCar.readJson(input);
        }
    }

public:
    static void parse(ExtContext &context, json &input) {
        auto &params = input["params"];
        auto &myCar = params["my_car"];
        int pos = myCar[2];
        parseCar(context, myCar);
        parseCar(context, params["enemy_car"]);
        if (pos == 1) {
            context.myCar = &context.leftCar;
            context.enemyCar = &context.rightCar;
        } else {
            context.myCar = &context.rightCar;
            context.enemyCar = &context.leftCar;
        }
    }
};


#endif //ALMOSTAGARIO_INPUTPARSER_H
