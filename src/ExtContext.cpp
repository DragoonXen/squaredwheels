//
// Created by dragoon on 4/7/18.
//

#include "ExtContext.h"
#include <chipmunk/chipmunk.h>
#include <chipmunk/chipmunk_structs.h>
#include "Constants.h"

void ExtContext::addGameBox(cpSpace *space) {
    // TODO: add sensor
    cpShapeSetSensor(
            shapes[0] = cpSpaceAddShape(space, cpSegmentShapeNew(cpSpaceGetStaticBody(space),
                                                                 cpv(0, 0),
                                                                 cpv(0, Constants::MAX_HEIGHT),
                                                                 1)),
            true);

    cpShapeSetSensor(shapes[1] = cpSpaceAddShape(space, cpSegmentShapeNew(cpSpaceGetStaticBody(space),
                                                                          cpv(0, Constants::MAX_HEIGHT),
                                                                          cpv(Constants::MAX_WIDTH, Constants::MAX_HEIGHT),
                                                                          1)),
                     true);

    cpShapeSetSensor(
            shapes[2] = cpSpaceAddShape(space, cpSegmentShapeNew(cpSpaceGetStaticBody(space),
                                                                 cpv(Constants::MAX_WIDTH, Constants::MAX_HEIGHT),
                                                                 cpv(Constants::MAX_WIDTH, 0),
                                                                 1)),
            true);

    cpShapeSetSensor(
            shapes[3] = cpSpaceAddShape(space, cpSegmentShapeNew(cpSpaceGetStaticBody(space),
                                                                 cpv(Constants::MAX_WIDTH, 0),
                                                                 cpv(0, 0),
                                                                 1)),
            true);
}

void ExtContext::createSpace() {
    this->space = cpSpaceNew();
    cpSpaceSetGravity(this->space, cpv(0., -700.));
    cpSpaceSetDamping(this->space, 0.85);
    this->ticksToDeadline = Constants::TICKS_TO_DEADLINE;
}

ExtContext::ExtContext(int tickIndex) : Context(tickIndex),
                                        simTicks(std::list<int>()),
                                        turnTime(std::list<int>()),
                                        totalTicks(0),
                                        totalTurnTime(0),
                                        totalLength(0L) {
    createSpace();
    this->clearSpaceHashValue = this->space->shapeIDCounter;
}

void ExtContext::clearShapes() {
    this->ticksToDeadline = Constants::TICKS_TO_DEADLINE;
    if (deadLine != nullptr) {
        deadLine->removeFromSpace(this->space);
        delete deadLine;
    }
    for (auto &segment : this->segments) {
        cpSpaceRemoveShape(this->space, segment);
        cpShapeFree(segment);
    }
    this->segments.clear();
    this->leftCar.remove(this->space);
    this->rightCar.remove(this->space);
    cpSpaceRemoveShape(space, shapes[0]);
    cpSpaceRemoveShape(space, shapes[1]);
    cpSpaceRemoveShape(space, shapes[2]);
    cpSpaceRemoveShape(space, shapes[3]);
    this->clearSpaceHashValue = this->space->shapeIDCounter;
//    cpSpaceFree(this->space);
//    createSpace();
}

void ExtContext::updateParams(json &params) {
    auto &p = params["params"];
    this->enemyLives = p["enemy_lives"];
    this->myLives = p["my_lives"];
    if (!this->segments.empty()) {
        clearShapes();
    }
    addGameBox(space);

    auto &car = p["proto_car"];
    double bodyMass = car["car_body_mass"];
    double carBodyFriction = car["car_body_friction"];
    double carBodyElasticity = car["car_body_elasticity"];

    std::vector<cpVect> carPoly;
    for (auto &segment : car["car_body_poly"]) {
        carPoly.emplace_back(cpv(segment[0], segment[1]));
    }
    std::vector<cpVect> carButton;
    for (auto &segment : car["button_poly"]) {
        carButton.emplace_back(cpv(segment[0], segment[1]));
    }
    this->deadLine = new DeadLine(DeadLineType::ASC, 1800., 800.);

    createCar(bodyMass, carBodyFriction, carBodyElasticity, carPoly, carButton, 1, car, leftCar, cpv(300., 300.));
    createCar(bodyMass, carBodyFriction, carBodyElasticity, carPoly, carButton, -1, car, rightCar, cpv(900., 300.));
    for (auto &segment : p["proto_map"]["segments"]) {
        cpShape *shape = cpSegmentShapeNew(cpSpaceGetStaticBody(this->space),
                                           cpv(segment[0][0], segment[0][1]),
                                           cpv(segment[1][0], segment[1][1]),
                                           segment[2]);
        cpShapeSetElasticity(shape, 0.);
        cpShapeSetFriction(shape, 1.);
        this->segments.push_back(shape);
        cpSpaceAddShape(this->space, shape);
    }
    this->deadLine->addToSpace(this->space);
    leftCar.addToSpace(this->space);
    rightCar.addToSpace(this->space);
}

Wheel createCarWheel(std::string side, json &json, int posMul, bool squared, Car &car, cpVect point) {
    Wheel wheel;
    double wheel_mass = json[side + "_wheel_mass"];
    double wheel_radius = json[side + "_wheel_radius"];
    wheel.radius = wheel_radius;
    std::vector<double> wheel_position = json[side + "_wheel_position"];
    double wheel_friction = json[side + "_wheel_friction"];
    double wheel_elasticity = json[side + "_wheel_elasticity"];
    std::vector<double> wheel_joint = json[side + "_wheel_joint"];
    std::vector<double> wheel_damp_position = json[side + "_wheel_damp_position"];
    double wheel_damp_length = json[side + "_wheel_damp_length"];
    double wheel_damp_stiffness = json[side + "_wheel_damp_stiffness"];
    double wheel_damp_damping = json[side + "_wheel_damp_damping"];

    cpBody *wheelBody = cpBodyNew(wheel_mass, squared ?
                                              cpMomentForBox(wheel_mass, wheel_radius * 2., wheel_radius * 2.) :
                                              cpMomentForCircle(wheel_mass, 0., wheel_radius, cpv(0., 0.)));
    cpBodySetPosition(wheelBody, cpv(wheel_position[0] * posMul, wheel_position[1]));

    wheel.body = wheelBody;

    cpShape *wheelShape = squared ?
                          cpBoxShapeNew(wheelBody, wheel_radius * 2., wheel_radius * 2., 0.) :
                          cpCircleShapeNew(wheelBody, wheel_radius, cpv(0., 0.));
    cpShapeSetFilter(wheelShape, car.carGroup);
    cpShapeSetFriction(wheelShape, wheel_friction);
    cpShapeSetElasticity(wheelShape, wheel_elasticity);
    wheel.wheelShape = wheelShape;
    wheel.joint = cpPinJointNew(wheelBody, car.carBody, cpv(0., 0.), cpv(wheel_joint[0] * posMul, wheel_joint[1]));

    //CP_EXPORT cpConstraint* cpDampedSpringNew(cpBody *a, cpBody *b, cpVect anchorA, cpVect anchorB, cpFloat restLength, cpFloat stiffness, cpFloat damping);
    wheel.damp = cpDampedSpringNew(wheelBody,
                                   car.carBody,
                                   cpv(0., 0.),
                                   cpv(wheel_damp_position[0] * posMul, wheel_damp_position[1]),
                                   wheel_damp_length,
                                   wheel_damp_stiffness,
                                   wheel_damp_damping);

    cpVect arr[] = {cpv(0., 0.), cpv(0., 1.), cpv(wheel_radius * 2 * posMul, 1.), cpv(wheel_radius * 2 * posMul, 0.)};
    wheel.stop = cpPolyShapeNew(car.carBody,
                                4,
                                &arr[0],
                                cpTransformNew(1., 0., 0., 1., wheel_damp_position[0] * posMul - wheel_radius * posMul, wheel_damp_position[1]),
                                0.);

    cpBodySetPosition(wheelBody, cpv(point.x + wheel_position[0] * posMul, point.y + wheel_position[1]));

    wheel.motor = nullptr;
    int drive = json["drive"];
    if (side[0] == 'r') { // rear
        if (drive == 1) {
            return wheel;
        }
    } else { // front
        if (drive == 2) {
            return wheel;
        }
    }
    wheel.motor = cpSimpleMotorNew(wheelBody, car.carBody, 0.);

    return wheel;
}

void
ExtContext::createCar(double bodyMass,
                      double carBodyFriction,
                      double carBodyElasticity,
                      std::vector<cpVect> carPoly,
                      std::vector<cpVect> carButton,
                      int posMul,
                      json &params,
                      Car &car,
                      cpVect point) {
    car.max_speed = params["max_speed"];
    car.torque = params["torque"];
    for (int i = 0; i != carPoly.size(); ++i) {
        carPoly[i] = cpv(carPoly[i].x * posMul, carPoly[i].y);
    }
    for (int i = 0; i != carButton.size(); ++i) {
        carButton[i] = cpv(carButton[i].x * posMul, carButton[i].y);
    }
    cpTransform trans = cpTransformNew(1., 0., 0., 1., 0., 0.);

    //CP_EXPORT cpFloat cpMomentForPoly(cpFloat m, int count, const cpVect *verts, cpVect offset, cpFloat radius);
    cpFloat moment = cpMomentForPoly(bodyMass, (int) carPoly.size(), &carPoly[0], cpv(0., 0.), 0.);
    cpBody *carBody = cpBodyNew(bodyMass, moment);
    car.carBody = carBody;

    //CP_EXPORT cpShape* cpPolyShapeNew(cpBody *body, int count, const cpVect *verts, cpTransform transform, cpFloat radius);
    cpShape *carShape = cpPolyShapeNew(carBody, (int) carPoly.size(), &carPoly[0], trans, 0.);
    car.carShape = carShape;
    cpShapeSetFriction(carShape, carBodyFriction);
    cpShapeSetElasticity(carShape, carBodyElasticity);
    int carGroup = (3 - posMul) / 2;
    car.carGroup = cpShapeFilterNew(carGroup, 0xffffffff, 0xffffffff);
    cpShapeSetFilter(carShape, car.carGroup);

    // create button shape
    cpShape *carButtonShape = cpPolyShapeNew(carBody, (int) carButton.size(), &carButton[0], trans, 0.);
    cpShapeSetFilter(carButtonShape, car.carGroup);
    cpShapeSetSensor(carButtonShape, true);
    cpShapeSetCollisionType(carButtonShape, carGroup * 10);
    car.carButtonShape = carButtonShape;

    // set center of gravity
    cpVect gravityCenter = cpShapeGetCenterOfGravity(carShape);
    cpBodySetCenterOfGravity(carBody, gravityCenter);

    //(std::string &side, json &json, int posMul, bool squared, cpShapeFilter &filter, cpBody *carBody) {
    bool squaredWheels = false;
    if (params.find("squared_wheels") != params.end()) {
        squaredWheels = params["squared_wheels"];
    }
    car.rearWheel = createCarWheel("rear", params, posMul, squaredWheels, car, point);
    car.frontWheel = createCarWheel("front", params, posMul, squaredWheels, car, point);
    cpBodySetPosition(carBody, point);
}

cpSpace *ExtContext::getSpace() {
    return this->space;
}

DeadLine *ExtContext::getDeadLine() {
    return this->deadLine;
}

void ExtContext::tick() {
    if (this->ticksToDeadline < 1) {
        this->deadLine->move();
    } else {
        --this->ticksToDeadline;
    }
    cpSpaceStep(this->space, 0.016);
}