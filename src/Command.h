//
// Created by dragoon on 8/31/18.
//

#ifndef SQUAREDWHEELS_COMMAND_H
#define SQUAREDWHEELS_COMMAND_H

#include <string>
#include "../../nlohmann/json.hpp"

using json = nlohmann::json;

enum MoveDirection {
    Left, Right, Stop
};

class Command {
    std::string debug = "";
    MoveDirection moveDirection;

public:
    Command(MoveDirection moveDirection);

    Command(MoveDirection moveDirection, const std::string &debug);

    void appendDebug(std::string &append);

    void appendDebug(std::string append);

    inline MoveDirection getMoveDirection() {
        return moveDirection;
    }

    json toJson();
};


#endif //SQUAREDWHEELS_COMMAND_H
