//
// Created by dragoon on 3/27/18.
//

#include "Context.h"

int Context::getTickIndex() const {
    return tickIndex;
}

void Context::nextTick() {
    ++this->tickIndex;
}

Context::Context(int tickIndex) : tickIndex(tickIndex) {
}