//
// Created by dragoon on 4/7/18.
//

#ifndef ALMOSTAGARIO_EXTCONTEXT_H
#define ALMOSTAGARIO_EXTCONTEXT_H

#include "Context.h"
#include "SimpleLog.h"
#include <list>
#include <vector>

#include "../nlohmann/json.hpp"
#include "model/MapSegment.h"
#include <chipmunk/chipmunk.h>
#include "model/Car.h"
#include "model/DeadLine.h"

using json = nlohmann::json;

class ExtContext : public Context {

private:
    int ticksToDeadline;
    cpHashValue clearSpaceHashValue;
    cpSpace *space;
    cpShape* shapes[4];
    DeadLine* deadLine;
    int enemyLives, myLives;

public:
    Car leftCar;
    Car rightCar;

    Car *myCar;
    Car *enemyCar;
    std::vector<cpShape *> segments;

    ExtContext(int tickIndex);

    void updateParams(json &params);

    std::list<int> simTicks;
    std::list<int> turnTime;
    int totalTicks;
    int totalTurnTime;
    long totalLength;

    void tick();

    cpSpace *getSpace();
    DeadLine* getDeadLine();

private:
    void createCar(double bodyMass,
                   double carBodyFriction,
                   double carBodyElasticity,
                   std::vector<cpVect> carPoly,
                   std::vector<cpVect> carButton,
                   int posMul,
                   json &params,
                   Car &car,
                   cpVect point);
    void addGameBox(cpSpace *space);
    void clearShapes();
    void createSpace();
};


#endif //ALMOSTAGARIO_EXTCONTEXT_H
