//
// Created by dragoon on 3/27/18.
//

#ifndef ALMOSTAGARIO_CONTEXT_H
#define ALMOSTAGARIO_CONTEXT_H

class Context {
public:
    int tickIndex;

    Context(int tickIndex);

    Context(const Context &obj) {
        this->tickIndex = obj.tickIndex;
    }

    int getTickIndex() const;

    void nextTick();
};


#endif //ALMOSTAGARIO_CONTEXT_H
