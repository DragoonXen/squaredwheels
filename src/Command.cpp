//
// Created by dragoon on 8/31/18.
//

#include "Command.h"

Command::Command(MoveDirection moveDirection) {
    this->moveDirection = moveDirection;
}

Command::Command(MoveDirection moveDirection, const std::string &debug) {
    this->moveDirection = moveDirection;
    this->debug = debug;
}

void Command::appendDebug(std::string &append) {
    this->debug += append;
}

void Command::appendDebug(std::string append) {
    this->debug += append;
}

json Command::toJson() {
    json j;

    switch (moveDirection) {
        case MoveDirection::Left:
            j["command"]="left";
            break;
        case MoveDirection::Right:
            j["command"]="right";
            break;
        case MoveDirection::Stop:
            j["command"]="stop";
            break;
    }
    if (!debug.empty()) {
        j["Debug"] = debug;
    }
    return j;
}