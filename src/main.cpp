#include "../nlohmann/json.hpp"
#include <iostream>
#include <fstream>
#include "Context.h"
#include "Solver.h"
#include "SimpleLog.h"
#include <chrono>

#ifdef REWIND_VIEWER

#include "../rewind/RewindClient.h"

#endif

#include "ExtContext.h"
#include <chipmunk/chipmunk_structs.h>

using json = nlohmann::json;

#ifdef REWIND_VIEWER

inline void drawShape(cpShape *shape, uint32_t color) {
    RewindClient &instance = RewindClient::instance();
    switch (shape->klass->type) {
        case cpShapeType::CP_SEGMENT_SHAPE: {
            cpVect vc1 = cpBodyLocalToWorld(cpShapeGetBody(shape), cpSegmentShapeGetA(shape));
            cpVect vc2 = cpBodyLocalToWorld(cpShapeGetBody(shape), cpSegmentShapeGetB(shape));
            cpFloat radius = cpSegmentShapeGetRadius(shape);
            if (radius == 0) {
                instance.line(vc1.x, vc1.y, vc2.x, vc2.y, color);
                return;
            }
            float dx = vc2.x - vc1.x;
            float dy = vc2.y - vc1.y;
            float dist = sqrt(dx * dx + dy * dy);
            dx /= dist;
            dy /= dist;
            dx *= radius;
            dy *= radius;
            instance.line(vc1.x - dy, vc1.y + dx, vc2.x - dy, vc2.y + dx, color);
            instance.line(vc1.x + dy, vc1.y - dx, vc2.x + dy, vc2.y - dx, color);
            instance.circle(vc1.x, vc1.y, radius, color);
            instance.circle(vc2.x, vc2.y, radius, color);
        }
            break;
        case cpShapeType::CP_POLY_SHAPE: {
            int vertCnt = cpPolyShapeGetCount(shape);
            cpVect first = cpBodyLocalToWorld(shape->body, cpPolyShapeGetVert(shape, 0));
            cpVect prev = first;
            for (int i = 1; i < vertCnt; ++i) {
                cpVect curr = cpBodyLocalToWorld(shape->body, cpPolyShapeGetVert(shape, i));

                instance.line(prev.x, prev.y, curr.x, curr.y, color);
                prev = curr;
            }
            instance.line(prev.x, prev.y, first.x, first.y, color);
        }
            break;
        case cpShapeType::CP_CIRCLE_SHAPE: {
            cpVect centr = cpBodyLocalToWorld(shape->body, cpCircleShapeGetOffset(shape));

            double radius = cpCircleShapeGetRadius(shape);
            instance.circle(centr.x, centr.y, radius, color);
            double angle = cpBodyGetAngle(shape->body);

            double xV = 12. * cos(angle) + centr.x;
            double yV = 12. * sin(angle) + centr.y;

            instance.line(centr.x, centr.y, xV, yV, 0xFFFFFF);
        }
            break;
    }
}

void drawSegmentShapeFunc(cpBody *body, cpShape *shape, uint32_t *color) {
    drawShape(shape, *color);
}

inline void drawPinJoint(cpConstraint *contrtaint, uint32_t color) {
    cpVect anA = cpPinJointGetAnchorA(contrtaint);
    cpBody *bodyA = cpConstraintGetBodyA(contrtaint);
    cpVect anB = cpPinJointGetAnchorB(contrtaint);
    cpBody *bodyB = cpConstraintGetBodyB(contrtaint);

    anA = cpBodyLocalToWorld(bodyA, anA);
    anB = cpBodyLocalToWorld(bodyB, anB);
    RewindClient &instance = RewindClient::instance();
    instance.line(anA.x, anA.y, anB.x, anB.y, color);
}

inline void drawDampedSpring(cpConstraint *contrtaint, uint32_t color) {
    cpVect anA = cpDampedSpringGetAnchorA(contrtaint);
    cpBody *bodyA = cpConstraintGetBodyA(contrtaint);
    cpVect anB = cpDampedSpringGetAnchorB(contrtaint);
    cpBody *bodyB = cpConstraintGetBodyB(contrtaint);

    anA = cpBodyLocalToWorld(bodyA, anA);
    anB = cpBodyLocalToWorld(bodyB, anB);
    RewindClient &instance = RewindClient::instance();
    instance.line(anA.x, anA.y, anB.x, anB.y, color);
}

void drawRealCar(const Car &car) {
    RewindClient &instance = RewindClient::instance();
    double radius = 12.;
    double xV = 12. * cos(car.rA) + car.rX;
    double yV = 12. * sin(car.rA) + car.rY;
    instance.circle(car.rX, car.rY, radius, 0x888888, 1);
    instance.line(car.rX, car.rY, xV, yV, 0xFFFFFF);

    xV = 12. * cos(car.fA) + car.fX;
    yV = 12. * sin(car.fA) + car.fY;
    instance.circle(car.fX, car.fY, radius, 0x888888, 1);
    instance.line(car.fX, car.fY, xV, yV, 0xFFFFFF);
}

void drawCar(const Car &car) {
    drawShape(car.carShape, 0x8888FF);
    drawShape(car.carButtonShape, 0x88FF88);
    drawShape(car.frontWheel.wheelShape, 0x444444);
    drawShape(car.frontWheel.stop, 0x000000);
    drawShape(car.rearWheel.wheelShape, 0x444444);
    drawShape(car.rearWheel.stop, 0x000000);
    drawPinJoint(car.frontWheel.joint, 0x000000);
    drawPinJoint(car.rearWheel.joint, 0x000000);
    drawDampedSpring(car.rearWheel.damp, 0xFF0000);
    drawDampedSpring(car.frontWheel.damp, 0xFF0000);
}

#endif

int main(int argc, char *argv[]) {
    srand(270718);
#ifdef LOCAL_RUN
    std::fstream file;
    if (argc == 1) {
        file.open("data.json", std::ios::out);
    } else {
        file.open(argv[1], std::ios::in);
    }
    std::vector<std::pair<ExtContext, std::string>> contexts;
#endif
    std::string data;
    double sum = 0.;
    ExtContext context(0);
    Solver solver;
    int micros = 0;
    int totalMicros = 0;
    while (true) {
        LOG_DEBUG("%", context.getTickIndex())
#ifdef LOCAL_RUN
        if (argc == 1) {
            getline(std::cin, data);
            file << data << std::endl;
        } else {
            if (file.eof()) {
                break;
            }
            getline(file, data);
        }
        LOG_INFO("%", data);
        contexts.emplace_back(context, data);
#else
        getline(std::cin, data);
#endif

        using std::chrono::system_clock;
        system_clock::time_point start = system_clock::now();
        if (data.size() == 0) {
            break;
        }
        auto parsed = json::parse(data);
        if (parsed["type"].get<std::string>() == "new_match") {
            context.updateParams(parsed);
            continue;
        }//0.0761716962
#ifdef REWIND_VIEWER
        RewindClient &instance = RewindClient::instance();
        cpSpace *space = context.getSpace();
//        for (auto &segment : context.segments) {
//            drawShapeFunc(segment, &instance);
//        }
//        cpBodyEachShape(cpSpaceGetStaticBody(space), reinterpret_cast<cpBodyShapeIteratorFunc>(drawShapeFunc), &instance);
//        cpSpaceEachShape(space, reinterpret_cast<cpSpaceShapeIteratorFunc>(drawShapeFunc), &instance);
        uint32_t color = 0x000000;
        cpBodyEachShape(cpSpaceGetStaticBody(space), reinterpret_cast<cpBodyShapeIteratorFunc>(drawSegmentShapeFunc), &color);
        drawShape(context.getDeadLine()->getShape(), 0x00FF00);
        drawCar(context.leftCar);
        drawCar(context.rightCar);
#endif
        auto command = solver.onTick(context, parsed);
#ifdef REWIND_VIEWER
        cpVect vc = cpBodyGetPosition(context.leftCar.frontWheel.body);
        double a = cpBodyGetAngle(context.leftCar.frontWheel.body);
        cpVect vc2 = cpBodyGetPosition(context.leftCar.rearWheel.body);
        double a2 = cpBodyGetAngle(context.leftCar.rearWheel.body);
        sum += abs(context.leftCar.fX - vc.x);
        sum += abs(context.leftCar.fY - vc.y);
        sum += abs(a - context.leftCar.fA);
        sum += abs(context.leftCar.rX - vc2.x);
        sum += abs(context.leftCar.rY - vc2.y);
        sum += abs(a2 - context.leftCar.rA);
        instance.message(R"(%.10e %.10e\n%.10e\n%.10e %.10e\n%.10e\n)",
                         context.leftCar.fX - vc.x,
                         context.leftCar.fY - vc.y,
                         a - context.leftCar.fA,
                         context.leftCar.rX - vc2.x,
                         context.leftCar.rY - vc2.y,
                         a2 - context.leftCar.rA);
        vc = cpBodyGetPosition(context.rightCar.frontWheel.body);
        a = cpBodyGetAngle(context.rightCar.frontWheel.body);
        vc2 = cpBodyGetPosition(context.rightCar.rearWheel.body);
        a2 = cpBodyGetAngle(context.rightCar.rearWheel.body);
        sum += abs(context.rightCar.fX - vc.x);
        sum += abs(context.rightCar.fY - vc.y);
        sum += abs(a - context.rightCar.fA);
        sum += abs(context.rightCar.rX - vc2.x);
        sum += abs(context.rightCar.rY - vc2.y);
        sum += abs(a2 - context.rightCar.rA);
        instance.message(R"(%.10e %.10e\n%.10e\n%.10e %.10e\n%.10e\n%10e\n)",
                         context.rightCar.fX - vc.x,
                         context.rightCar.fY - vc.y,
                         a - context.rightCar.fA,
                         context.rightCar.rX - vc2.x,
                         context.rightCar.rY - vc2.y,
                         a2 - context.rightCar.rA,
                         sum);
        drawRealCar(context.leftCar);
        drawRealCar(context.rightCar);
        instance.end_frame();
#endif
        system_clock::time_point end = system_clock::now();
        command.appendDebug(" time: " + std::to_string(micros));

        std::cout << command.toJson().dump() << std::endl;
#ifdef PERF
        std::cout << context.getTickIndex() << std::endl;
#endif
        micros = std::chrono::duration_cast<std::chrono::microseconds>(end - start).count();
        totalMicros += micros;
        LOG_DEBUG("%", command.toJson().dump())
        context.nextTick();
        switch (command.getMoveDirection()) {
            case Left:
                context.myCar->goLeft(context.getSpace());
                break;
            case Right:
                context.myCar->goRight(context.getSpace());
                break;
            case Stop:
                context.myCar->stop();
                break;
        }
        context.tick();
    }

#ifdef LOCAL_RUN
#ifndef PERF
    file.close();
    std::cout << Command(MoveDirection::Stop, "dead. avg time: " + std::to_string(((double) totalMicros) / (context.getTickIndex() + 1))).toJson().dump()
              << std::endl;
    int idxToDebug;
    while (true) {
        std::cout << "type index of debug tick (exit if negative)" << std::endl;
        std::cin >> idxToDebug;
        if (idxToDebug < 0) {
            break;
        }
        ExtContext contextCopy = contexts[idxToDebug].first;
        auto dataCopy = json::parse(contexts[idxToDebug].second);
        auto command = solver.onTick(contextCopy, dataCopy);

        std::cout << command.toJson().dump() << std::endl;
#ifdef REWIND_VIEWER
        RewindClient &instance = RewindClient::instance();
        instance.end_frame();
#endif
    }
#endif
#endif
    return 0;
}