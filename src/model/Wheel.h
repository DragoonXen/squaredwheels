//
// Created by dragoon on 9/1/18.
//

#ifndef MADSQUAREDWHEELS_WHEEL_H
#define MADSQUAREDWHEELS_WHEEL_H


#include <vector>
#include <chipmunk/chipmunk.h>

struct Wheel {
    cpBody *body;
    cpShape *wheelShape;
    cpConstraint *joint;
    cpConstraint *damp;
    cpShape *stop;
    cpConstraint *motor;

    double radius;

    void addObjectsToSpace(cpSpace *space) {
        cpSpaceAddShape(space, wheelShape);
        cpSpaceAddConstraint(space, joint);
        cpSpaceAddConstraint(space, damp);
        cpSpaceAddShape(space, stop);
    }

    void remove(cpSpace *space) {
        if (motor != nullptr) {
            cpSpaceRemoveConstraint(space, motor);
            cpConstraintFree(motor);
        }
        motor = nullptr;
        cpSpaceRemoveShape(space, stop);
        cpShapeFree(stop);

        cpSpaceRemoveConstraint(space, damp);
        cpConstraintFree(damp);
        cpSpaceRemoveConstraint(space, joint);
        cpConstraintFree(joint);

        cpSpaceRemoveShape(space, wheelShape);
        cpShapeFree(wheelShape);

        cpSpaceRemoveBody(space, body);
        cpBodyFree(body);
    }
};


#endif //MADSQUAREDWHEELS_WHEEL_H
