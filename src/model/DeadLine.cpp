//
// Created by dragoon on 9/3/18.
//

#include "DeadLine.h"

DeadLine::DeadLine(DeadLineType type, double length, double height) {
    this->type = type;
    this->body = cpBodyNewKinematic();
    this->shape = cpSegmentShapeNew(this->body, cpv(0., 0.), cpv(length, 0.), 2.);
    cpBodySetPosition(this->body, cpv(0., type == DeadLineType::ASC ? 10. : height - 10.));
    cpShapeSetSensor(this->shape, true);
}

void DeadLine::addToSpace(cpSpace *space) {
    cpSpaceAddShape(space, this->shape);
}

void DeadLine::move() {
    cpBody* chapeBody = cpShapeGetBody(this->shape);
    cpVect position = cpBodyGetPosition(chapeBody);
    if (type == DeadLineType::ASC) {
        cpBodySetPosition(chapeBody, cpv(position.x, position.y + 0.5));
    } else {
        cpBodySetPosition(chapeBody, cpv(position.x, position.y - 0.5));
    }
}

void DeadLine::removeFromSpace(cpSpace *space) {
    cpSpaceRemoveShape(space, this->shape);
    cpShapeFree(this->shape);
    cpBodyFree(this->body);
}

cpShape *DeadLine::getShape() {
    return this->shape;
}
