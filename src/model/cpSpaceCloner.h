//
// Created by dragoon on 9/4/18.
//

#ifndef MADSQUAREDWHEELS_CPSPACECLONER_H
#define MADSQUAREDWHEELS_CPSPACECLONER_H


#include <chipmunk/chipmunk_structs.h>

class cpSpaceCloner {
    cpSpace clonedSpace;
    cpSpace* defaultSpace;
    cpSpaceCloner(cpSpace* space);
    void releaseSpace();
};


#endif //MADSQUAREDWHEELS_CPSPACECLONER_H
