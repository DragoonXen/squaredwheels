//
// Created by dragoon on 8/31/18.
//

#ifndef MADSQUAREDWHEELS_MAPSEGMENT_H
#define MADSQUAREDWHEELS_MAPSEGMENT_H


class MapSegment {
    double start[2], end[2], thickness;

public:
    MapSegment(double sX, double sY, double fX, double fY, double thickness);
};


#endif //MADSQUAREDWHEELS_MAPSEGMENT_H
