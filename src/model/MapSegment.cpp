//
// Created by dragoon on 8/31/18.
//

#include "MapSegment.h"

MapSegment::MapSegment(double sX, double sY, double fX, double fY, double thickness) : thickness(thickness) {
    this->start[0] = sX;
    this->start[1] = sY;
    this->end[0] = fX;
    this->end[1] = fY;
}
