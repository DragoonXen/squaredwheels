//
// Created by dragoon on 9/3/18.
//

#ifndef MADSQUAREDWHEELS_DEADLINE_H
#define MADSQUAREDWHEELS_DEADLINE_H


#include <chipmunk/chipmunk.h>

enum DeadLineType {
    ASC, DESC
};

class DeadLine {
    cpBody *body;
    cpShape *shape;
    DeadLineType type;
public:
    DeadLine(DeadLineType type, double length, double height);

    void addToSpace(cpSpace *space);

    void move();

    void removeFromSpace(cpSpace *space);

    cpShape* getShape();
};


#endif //MADSQUAREDWHEELS_DEADLINE_H
