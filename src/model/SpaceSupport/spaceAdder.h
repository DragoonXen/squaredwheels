//
// Created by dragoon on 9/4/18.
//

#ifndef MADSQUAREDWHEELS_SPACEADDER_H
#define MADSQUAREDWHEELS_SPACEADDER_H


#include <chipmunk/chipmunk_structs.h>

class spaceAdder {

    virtual void addItemToSpace(void* item, cpSpace* space);

};


#endif //MADSQUAREDWHEELS_SPACEADDER_H
