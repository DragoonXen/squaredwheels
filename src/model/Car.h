//
// Created by dragoon on 9/1/18.
//

#ifndef MADSQUAREDWHEELS_CAR_H
#define MADSQUAREDWHEELS_CAR_H


#include <vector>
#include <chipmunk/chipmunk.h>
#include "Wheel.h"
#include "../../nlohmann/json.hpp"

using json = nlohmann::json;

struct Car {
    cpBody *carBody;
    cpShape *carShape;
    cpShape *carButtonShape;
    Wheel frontWheel;
    Wheel rearWheel;

    std::vector<cpConstraint *> motors;

    double max_speed, torque;
    cpShapeFilter carGroup;

    double pX, pY;
    double angle;
    double fX, fY, fA;
    double rX, rY, rA;

    void remove(cpSpace *space) {
        frontWheel.remove(space);
        rearWheel.remove(space);
        cpSpaceRemoveShape(space, carShape);
        cpShapeFree(carShape);
        cpSpaceRemoveShape(space, carButtonShape);
        cpShapeFree(carButtonShape);

        cpSpaceRemoveBody(space, carBody);
        cpBodyFree(carBody);
    }

    void goRight(cpSpace *space) {
        if (inAir(space)) {
            cpBodySetTorque(carBody, this->torque);
        }
        for (auto &motor : motors) {
            cpSimpleMotorSetRate(motor, -max_speed);
        }
    }

    void goLeft(cpSpace *space) {
        if (inAir(space)) {
            cpBodySetTorque(carBody, -this->torque);
        }
        for (auto &motor : motors) {
            cpSimpleMotorSetRate(motor, max_speed);
        }
    }

    void stop() {
        for (auto &motor : motors) {
            cpSimpleMotorSetRate(motor, 0);
        }
    }

    bool inAir(cpSpace *space) {
        return (cpSpacePointQueryNearest(space, cpBodyGetPosition(rearWheel.body), rearWheel.radius + 1., carGroup, nullptr) == nullptr &&
            cpSpacePointQueryNearest(space, cpBodyGetPosition(frontWheel.body), frontWheel.radius + 1., carGroup, nullptr) == nullptr);
    }

//    def in_air(self):
//    return not (self.point_query_nearest(self.rear_wheel_body.position, self.rear_wheel_radius + 1, pymunk.ShapeFilter(group=self.car_group))
//    or self.point_query_nearest(self.front_wheel_body.position, self.front_wheel_radius + 1, pymunk.ShapeFilter(group=self.car_group)))


    /*
     *     def go_right(self):
        if self.in_air():
            self.car_body.torque = self.torque

        for motor in self.motors:
            motor.rate = -self.max_speed

    def go_left(self):
        if self.in_air():
            self.car_body.torque = -self.torque

        for motor in self.motors:
            motor.rate = self.max_speed

    def stop(self):
        for motor in self.motors:
            motor.rate = 0
     */

    void addToSpace(cpSpace *space) {
        cpSpaceAddShape(space, carButtonShape);
        cpSpaceAddBody(space, carBody);
        cpSpaceAddShape(space, carShape);
        cpSpaceAddBody(space, rearWheel.body);
        cpSpaceAddBody(space, frontWheel.body);

        rearWheel.addObjectsToSpace(space);
        frontWheel.addObjectsToSpace(space);
//    cpCollisionHandler* cHandler = cpSpaceAddWildcardHandler(space, carGroup * 10);

        motors.clear();
        if (rearWheel.motor != nullptr) {
            motors.push_back(rearWheel.motor);
            cpSpaceAddConstraint(space, rearWheel.motor);
        }
        if (frontWheel.motor != nullptr) {
            motors.push_back(frontWheel.motor);
            cpSpaceAddConstraint(space, frontWheel.motor);
        }
    }

    void readJson(json &params) {
        std::vector<double> myCarCoords = params[0];
        pX = myCarCoords[0];
        pY = myCarCoords[1];
        angle = params[1];
        std::vector<double> rearWheel = params[3];
        rX = rearWheel[0];
        rY = rearWheel[1];
        rA = rearWheel[2];
        std::vector<double> frontWheel = params[4];
        fX = frontWheel[0];
        fY = frontWheel[1];
        fA = frontWheel[2];
    }
};


#endif //MADSQUAREDWHEELS_CAR_H
